#!/bin/bash

# Configure Apache.
cp -f DrupalCI/src/gitlab/000-default.conf /etc/apache2/sites-available
service apache2 restart

mkdir sqlsrv_drupalci

# Install Drupal Database Driver
#    From Gitlab
apt-get -y install git
git clone -b 4.1.x  https://git.drupalcode.org/project/sqlsrv.git sqlsrv_drupalci/sqlsrv

#    From Drupal.org
#curl -fSL https://ftp.drupal.org/files/projects/sqlsrv-4.1.x-dev.tar.gz -o sqlsrv.tar.gz
#tar -xzf sqlsrv.tar.gz -C sqlsrv_drupalci
#rm sqlsrv.tar.gz

cp -R sqlsrv_drupalci/sqlsrv /var/www/html/modules

# Configure Drupal
cp DrupalCI/src/settings.php /var/www/html/sites/default/

ls -al /var/www/html/core
mv /var/www/html/core/phpunit.xml.dist /var/www/html/core/phpunit.xml

# Install  and configure Database
sleep 15
sqlcmd -P Password12! -S localhost -U SA -Q "CREATE DATABASE mydrupalsite COLLATE LATIN1_GENERAL_100_CI_AS_SC_UTF8"

cp -r /var/www/html/modules/sqlsrv/tests/database_statement_monitoring_test /var/www/html/core/modules/system/tests/modules/database_statement_monitoring_test/src/sqlsrv

sqlcmd -P Password12! -S localhost -U SA -d mydrupalsite -Q "EXEC sp_configure 'show advanced options', 1; RECONFIGURE; EXEC sp_configure 'clr strict security', 0; RECONFIGURE; EXEC sp_configure 'clr enable', 1; RECONFIGURE"
sqlcmd -P Password12! -S localhost -U SA -d mydrupalsite -Q "CREATE ASSEMBLY Regex from '/var/opt/mssql/data/RegEx.dll' WITH PERMISSION_SET = SAFE"
sqlcmd -P Password12! -S localhost -U SA -d mydrupalsite -Q "CREATE FUNCTION dbo.REGEXP(@pattern NVARCHAR(100), @matchString NVARCHAR(100)) RETURNS bit EXTERNAL NAME Regex.RegExCompiled.RegExCompiledMatch"

cd /var/www/html

# Install PHPCS standard
export COMPOSER_MEMORY_LIMIT=-1
composer global require drupal/coder
/opt/drupal/vendor/squizlabs/php_codesniffer/bin/phpcs --config-set installed_paths vendor/drupal/coder/coder_sniffer

# patch files
# Enable sqlsrv module in specific kernel tests
curl -fSL https://www.drupal.org/files/issues/2020-05-02/2966272-16.patch -o 2966272.patch
git apply 2966272.patch

# ConnectionUnitTest defaults to MySQL syntax
curl -fSL http://beakerboy.com/~kevin/connectionUnit.patch -o connectionUnit.patch
git apply connectionUnit.patch

